var fx = (x,p) => (2*x + 7) % p;
var fy = (y,p) => (3*y + 13) % p;
function weakPrng(prime,seed){
    var p = prime;
    var x = seed.x;
    var y = seed.y;
    this.proxima = function(){
        x = fx(x,p);
        y = fy(y,p);
 return (x ^ y);
 };
}                               // ex 3      // ex 4
P = 134221729     // 134221729 // 199996207 // 199991257
seedx = 72843475  // 72843475  // 145686953 // 225693
seedy = 33543001  // 33543001  // 112668993 // 342676
seed={ x:seedx, y:seedy };
prng =new weakPrng(P,seed);
output=[];
for (i=0;i<9;i++)
 output[i]=prng.proxima();
for (i=0;i<9;i++)
 console.log( "%s",output[i] );
